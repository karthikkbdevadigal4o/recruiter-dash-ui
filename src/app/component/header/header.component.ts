import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CommonDataService } from 'src/app/providers/common-data.service';
import { HttpserviceService } from 'src/app/providers/httpservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private router: Router, public httpService: HttpserviceService,public cds:CommonDataService) { }
  logout(){
    localStorage.clear();
    this.httpService.auth_token=null;
    this.httpService.userInfo=null;
    this.router.navigateByUrl("/home"); 
  }
  ngOnInit(): void {
  }

}

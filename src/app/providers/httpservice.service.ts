import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { CommonDataService } from './common-data.service';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {
  public serviceBase: any;
  public serviceBaseL4o: any;
  public serviceimg: any;
  public userInfo: any;
  public auth_token: any;
  constructor(private router: Router, public http: HttpClient, private toastrService: ToastrService) {
    if (localStorage.getItem('userTocken')) {
      this.auth_token = localStorage.getItem('userTocken');
    }
    if (localStorage.getItem('userInfo')) {
      this.userInfo = JSON.parse(localStorage.getItem('userInfo')!);
    }
    this.serviceBase = environment.apiUrl;
    this.serviceBaseL4o = environment.apiUrl1;
    this.serviceimg = environment.imageUrl;
  }
  public trimText(text: string, len: number) {
    return text?.length > len ? text.substring(0, len) + '...' : text;
  }
  createAuthorizationHeader() {
    if (localStorage.getItem("userTocken")) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("userTocken")!
      });
    }
    else {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }

  }

  getWithoutAuth(url: any) {
    return this.http.get(this.serviceBase + url, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  postWithoutAuth(url: any, data: any) {
    return this.http.post(this.serviceBase + url, data, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  goTo(page: any) {
    this.router.navigate([page]);
  }
  // API CALLs FOR L4O
  getL4o(url: any) {
    return this.http.get(this.serviceBaseL4o + url/* , { headers: this.createAuthorizationHeader() } */);
  }
  postL4o(url: any, data: any) {
    return this.http.post(this.serviceBaseL4o + url, data/* , { headers: this.createAuthorizationHeader() } */);
  }
  uploadImage(httpParams:any) {

    // let httpParams = new HttpParams();
    // Object.keys(image).forEach(function (key) {
    //   httpParams = httpParams.append(key, image[key]);
    // });
    return this.http.post(this.serviceBase + 'recruiters/upload_image', httpParams);

  }
  uploadFile(httpParams:any) {

    // let httpParams = new HttpParams();
    // Object.keys(image).forEach(function (key) {
    //   httpParams = httpParams.append(key, image[key]);
    // });
    return this.http.post(this.serviceBase + 'recruiters/upload_file', httpParams);

  }

  
  put(url: any, data: any) {
    return this.http.put(this.serviceBase + url, data/* , { headers: this.createAuthorizationHeader() } */);
  }


  showSuccess(message: any) {
    this.toastrService.success(message, "Success:");
  }

  showError(message: any) {
    this.toastrService.error(message, "Error:", { enableHtml: true });
  }

  showInfo(message: any) {
    this.toastrService.info(message);
  }

  showWarning(message: any) {
    this.toastrService.warning(message);
  }
}

import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from '../contrainer';
import { AccountComponent } from './account.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { OnboardingDetailComponent } from './onboarding-detail/onboarding-detail.component';
import { OnboardingDetailsComponent } from './onboarding-details/onboarding-details.component';
import { RegistrationSuccessMailComponent } from './registration-success-mail/registration-success-mail.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

const routes: Routes = [
  
  
  { path: '', component: AccountComponent },
  { path: 'onboarding', component: OnboardingDetailsComponent },
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'success-mail', component:RegistrationSuccessMailComponent},
  {path: 'new-password', component:NewPasswordComponent},
  {path: 'onboarding-two', component:OnboardingDetailComponent},
  {path: 'update-profile', component:UpdateProfileComponent},
  {path: 'change-password', component:ChangePasswordComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }

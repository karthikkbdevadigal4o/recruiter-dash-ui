import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpserviceService } from 'src/app/providers/httpservice.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  passwordForm: FormGroup;
  constructor(private router: Router, private httpService: HttpserviceService) {
    let userInfo = JSON.parse(localStorage.getItem("userInfo")!);
    this.passwordForm = new FormGroup({
      '_id': new FormControl(userInfo._id, [Validators.required]),
      'password': new FormControl('', [Validators.required]),
      'cpassword': new FormControl('', [Validators.required]),
      'old_password': new FormControl('', [Validators.required]),
    });
  }
  onSubmit() {
    let self = this;
    // console.log("API not implemented yet");
    let frmData = this.passwordForm.value
    if (frmData.password == frmData.cpassword) {
      delete frmData.cpassword;
      this.httpService.postWithoutAuth("recruiters/change_password", frmData).subscribe((res: any) => {
        if (res.type) {
          self.httpService.showSuccess(res.message);
          self.passwordForm.reset();
        }
        else {
          if (res.message) {
            this.httpService.showError(res.message);
          }
        }
      });
    }
    else{
      this.httpService.showError("Password and Re-type password does not matched.");
    }
  }
  ngOnInit(): void {
  }

}

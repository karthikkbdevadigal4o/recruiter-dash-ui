import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationSuccessMailComponent } from './registration-success-mail.component';

describe('RegistrationSuccessMailComponent', () => {
  let component: RegistrationSuccessMailComponent;
  let fixture: ComponentFixture<RegistrationSuccessMailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationSuccessMailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationSuccessMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

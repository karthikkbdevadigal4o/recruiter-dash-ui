import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpserviceService } from 'src/app/providers/httpservice.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {
  signupForm: FormGroup;
  constructor(private router: Router, private httpService: HttpserviceService) {
    this.signupForm = new FormGroup({
      '_id': new FormControl('', [Validators.required]),
      'name': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'phone': new FormControl('', [Validators.required]),
      'designation': new FormControl(''),
      'organisation': new FormControl(''),
      'website': new FormControl(''),
      'department': new FormControl(''),
      'logo': new FormControl('1'),
      'primary_job_category': new FormControl('1')
    });
  }
  getDetails() {
    let self = this;
    let userInfo=JSON.parse(localStorage.getItem("userInfo")!);
    this.httpService.postWithoutAuth("recruiters/user_details", {id:userInfo._id}).subscribe((res: any) => {
      if (res.type) {
        self.signupForm.patchValue(res.data)
        // self.httpService.showSuccess(res.message);
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
  onSubmit() {
    let self = this;
    // console.log("API not implemented yet");
    
    this.httpService.postWithoutAuth("recruiters/update_profile", this.signupForm.value).subscribe((res: any) => {
      if (res.type) {
        self.httpService.showSuccess(res.message);
        self.getDetails();
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
  ngOnInit(): void {
    this.getDetails()
  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpserviceService } from 'src/app/providers/httpservice.service';
declare var $:any;
@Component({
  selector: 'app-schedule-interview-popoup',
  templateUrl: './schedule-interview-popoup.component.html',
  styleUrls: ['./schedule-interview-popoup.component.scss']
})
export class ScheduleInterviewPopoupComponent implements OnInit {
  interviewForm: FormGroup;
  JobDetails: any;
  myTimePicker: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: { job_id: string, sector_id: any, candidate: any }, public httpService: HttpserviceService) {
    let userInfo = JSON.parse(localStorage.getItem("userInfo")!);
    console.log(userInfo, data);
    this.interviewForm = new FormGroup({
      "title": new FormControl('Round 0 interview with '+userInfo.name, [Validators.required]),
      "job_id": new FormControl(data.job_id),
      "job_category_id": new FormControl(''),
      "description": new FormControl('', [Validators.required]),
      "recruiter_id": new FormControl(userInfo._id),
      "candidate_id": new FormControl(data.candidate.user),
      "sector_id": new FormControl(data.sector_id),
      "job_description_doc": new FormControl('', [Validators.required]),
      "salary": new FormControl('', [Validators.required]),
      "start_time": new FormControl('', [Validators.required]),
      "remark": new FormControl('', [Validators.required]),
      "interview_date": new FormControl('', [Validators.required]),
      "interview_status": new FormControl("Interview Scheduled", [Validators.required]),
      "status": new FormControl(true, [Validators.required])
    })
    // console.log(this.interviewForm.value);
    this.fetchJobDetails(data.job_id)
  }
  fetchJobDetails(jobId: any) {
    let self = this;
    this.httpService.getWithoutAuth("jobs/" + jobId).subscribe((res: any) => {
      self.JobDetails = res;
      self.interviewForm.patchValue({
        "salary": self.JobDetails.salary,
        "job_category_id": self.JobDetails.job_category_id,
        "job_description_doc": self.JobDetails.job_description_doc
      })
      console.log(self.interviewForm.value);
    });
  }
  onSubmit() {
    let self = this;
    // console.log("API not implemented yet");

    this.httpService.postWithoutAuth("interviews", this.interviewForm.value).subscribe((res: any) => {
      if (res.type) {
        self.httpService.showSuccess(res.message);
        $("#close-modal").click();
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
  ngOnInit(): void {
  }

}

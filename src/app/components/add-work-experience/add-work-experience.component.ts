import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-work-experience',
  templateUrl: './add-work-experience.component.html',
  styleUrls: ['./add-work-experience.component.scss']
})
export class AddWorkExperienceComponent implements OnInit {
  frmData:any={"title":"","description":"","startDate":"","endDate":"","company":"","edit":false}
  error:any={isError:false,errorMessage:''};

  constructor(public dialogRef: MatDialogRef<AddWorkExperienceComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {
    if(this.data.edit)
      this.frmData=data
    console.log(JSON.stringify(this.frmData));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  compareTwoDates(){
    if(new Date(this.frmData.endDate)<new Date(this.frmData.startDate)){
       this.error={isError:true,errorMessage:"End Date can't before start date"};
    }
    else
    this.error={isError:false,errorMessage:""};
 }
  ngOnInit(): void {
  }

}

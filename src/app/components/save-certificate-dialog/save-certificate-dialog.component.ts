import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-save-certificate-dialog',
  templateUrl: './save-certificate-dialog.component.html',
  styleUrls: ['./save-certificate-dialog.component.scss']
})
export class SaveCertificateDialogComponent implements OnInit {
  frmData:any={"title":"","authority":"","description":"","date":"","edit":false}
  constructor(public dialogRef: MatDialogRef<SaveCertificateDialogComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {
    if(this.data.edit)
      this.frmData=data
    console.log(JSON.stringify(this.frmData));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

}

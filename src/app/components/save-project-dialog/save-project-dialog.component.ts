import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-save-project-dialog',
  templateUrl: './save-project-dialog.component.html',
  styleUrls: ['./save-project-dialog.component.scss']
})
export class SaveProjectDialogComponent implements OnInit {
  frmData:any={"title":"","subtitle":"","description":"","startDate":"","endDate":"","edit":false}
  constructor(public dialogRef: MatDialogRef<SaveProjectDialogComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {
    if(this.data.edit)
      this.frmData=data
    console.log(JSON.stringify(this.frmData));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

}

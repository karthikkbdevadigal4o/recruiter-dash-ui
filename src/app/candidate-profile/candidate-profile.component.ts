import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AddWorkExperienceComponent } from 'src/app/components/add-work-experience/add-work-experience.component';
import { SaveEducationDialogComponent } from 'src/app/components/save-education-dialog/save-education-dialog.component';
import { ScheduleInterviewPopoupComponent } from 'src/app/components/schedule-interview-popoup/schedule-interview-popoup.component';
import { SaveCertificateDialogComponent } from '../components/save-certificate-dialog/save-certificate-dialog.component';
import { SaveProjectDialogComponent } from '../components/save-project-dialog/save-project-dialog.component';
import { HttpserviceService } from './../providers/httpservice.service';
@Component({
  selector: 'app-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})
export class CandidateProfileComponent implements OnInit {
  pageContent: any;
  candidateId: any;
  selectedCandidateFilter: any = { status: "", sortType: "" }
  constructor(public dialog: MatDialog, private sanitizer: DomSanitizer, public httpService: HttpserviceService, private route: ActivatedRoute,) {

    this.route.params.subscribe(param => {
      this.candidateId = param.id
      this.fetchDetails(param.id)

    });
  }
  fetchDetails(id: any) {
    let self = this;
    this.httpService.getL4o("learner/job?userId=" + id).subscribe((res: any) => {
      // console.log(res);
      self.pageContent = res.jobApplication;
      if (self.pageContent?.videoResume)
        self.pageContent.videoResume = self.sanitizer.bypassSecurityTrustResourceUrl(this.pageContent?.videoResume + '&rel=0')
        console.log(self.pageContent.videoResume);
        
    })
  }
  openAddEducationDialog(item: any = null) {
    if (item != null)
      item.edit = true;
    else
      item = { edit: false }
    const dialogRef = this.dialog.open(SaveEducationDialogComponent, { restoreFocus: false, width: '70vw', minWidth: "320px", maxHeight: '90vh', data: item });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (!result.edit)
          this.pageContent.resume.education.push(result)
        this.saveResumeData();
      }
      console.log(result);
    });
  }
  removeEducation(index: any) {
    if (confirm("Are you sure,you want to delete this item?")) {
      this.pageContent.resume.education.splice(index, 1);
      this.saveResumeData();
    }
  }
  removeWorkExperience(index: any) {
    if (confirm("Are you sure,you want to delete this item?")) {
      this.pageContent.resume.work.splice(index, 1);
      this.saveResumeData();
    }
  }
  removeProject(index: any) {
    if (confirm("Are you sure,you want to delete this item?")) {
      this.pageContent.resume.project.splice(index, 1);
      this.saveResumeData();
    }
  }
  removeCertificate(index: any) {
    if (confirm("Are you sure,you want to delete this item?")) {
      this.pageContent.resume.certificate.splice(index, 1);
      this.saveResumeData();
    }
  }
  addworkexperience(item: any = null) {
    if (item != null)
      item.edit = true;
    else
      item = { edit: false }
    const dialogRef = this.dialog.open(AddWorkExperienceComponent, { restoreFocus: false, width: '70vw', minWidth: "320px", maxHeight: '90vh', data: item });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (!result.edit)
          this.pageContent.resume.work.push(result)
        this.saveResumeData();
      }
      console.log(result);
    });
  }
  addProject(item: any = null) {
    if (item != null)
      item.edit = true;
    else
      item = { edit: false }
    const dialogRef = this.dialog.open(SaveProjectDialogComponent, { restoreFocus: false, width: '70vw', minWidth: "320px", maxHeight: '90vh', data: item });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (!result.edit) {
          this.pageContent.resume.project.push(result)
        }
          this.saveResumeData();
      }
      console.log(result);
    });
  }
  addCertificate(item: any = null) {
    if (item != null)
      item.edit = true;
    else
      item = { edit: false }
    const dialogRef = this.dialog.open(SaveCertificateDialogComponent, { restoreFocus: false, width: '70vw', minWidth: "320px", maxHeight: '90vh', data: item });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (!result.edit) {
          this.pageContent.resume.certificate.push(result)
        }
          this.saveResumeData();
      }
      console.log(result);
    });
  }
  saveResumeData() {
    var reqData = this.pageContent.resume;
    reqData.userId = reqData.user
    reqData.resumeId = reqData.id
    delete reqData.createdAt
    delete reqData.updatedAt
    delete reqData.id
    delete reqData.user
    console.log(reqData);
    let self=this;
    this.httpService.postL4o("learner/resume/update", reqData).subscribe((res: any) => {
      this.fetchDetails(this.candidateId)
    });
    // this.httpService.postL4o("learner/resume/update",this.pageContent)
  }
  updateInterViewStatus(item: any) {
    let self = this;
    console.log(item)
    if (item.statusDetails._id) {
      this.httpService.postWithoutAuth("interviews/update_interview_status", { "_id": item.statusDetails._id, "interview_status": item.statusDetails.interview_status }).subscribe((res: any) => {
        if (res.type) {
          self.httpService.showSuccess(res.message);
        }
      });
    }
    else {
      var interviewDetails = {
        "title": "",
        "job_id": this.pageContent.selectedjobId,
        "job_category_id": "",
        "description": "",
        "recruiter_id": "",
        "candidate_id": item.user,
        "sector_id": this.pageContent.selectedFilter.sector,
        "job_description_doc": "",
        "salary": "",
        "start_time": "",
        "remark": "",
        "interview_date": "",
        "interview_status": item.statusDetails.interview_status,
        "status": true
      }
    }
  }
  scheduleInterviewPopup(item: any): void {
    let self = this;
    const dialogRef = this.dialog.open(ScheduleInterviewPopoupComponent, {
      minWidth: '320px', maxWidth: '40%', data: { job_id: this.pageContent, sector_id: this.pageContent.selectedFilter.sector, candidate: item },
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      // self.fetchApplications()
    });
  }


  ngOnInit(): void {
    console.log(this.pageContent);
  }

}

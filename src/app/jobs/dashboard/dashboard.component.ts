import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpserviceService } from 'src/app/providers/httpservice.service';
import { CommonDataService } from 'src/app/providers/common-data.service';
import { MatDialog } from '@angular/material/dialog';
import { AddRemarksPopoupComponent } from 'src/app/components/add-remarks-popoup/add-remarks-popoup.component';
import { ScheduleInterviewPopoupComponent } from 'src/app/components/schedule-interview-popoup/schedule-interview-popoup.component';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  disableSelect = new FormControl(false);
  showProfile = false;
  showNoCandidate = 0;
  currentActiveTab = "Jobs"
  selectedFilter: any = { "sector": "", "state": { "name": "", "id": "" }, "district": "", "userId": "", "skip": "", "limit": "" }
  selectedCandidateFilter: any = { status: "", sortType: "name" }
  selectedFilterStateIndex: any = "";
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 5
      }
    },
    nav: true
  }
  jobsList: any = [];

  sectoresList: any = [];
  applicationsList: any = [];
  filteredApplicationsList: any = [];
  statesList: any = [];
  districtsList: any = [];
  selectedProfile: any;
  selectedjobId: any = "";
  serchjobLocationTxt: any = "";
  // interviewListShow: any = false;
  constructor(private route: ActivatedRoute, private router: Router, public httpService: HttpserviceService, public cds: CommonDataService, public dialog: MatDialog, private sanitizer: DomSanitizer) {
    let self = this;
    this.route.params.subscribe(param => {
      if (param.listType == "candidates") {
        self.currentActiveTab = "Candidate"
      }
      else if (param.listType == "search") {
        self.currentActiveTab = "Search"
      }
      else if (param.listType == "jobs") {
        self.currentActiveTab = "Jobs"
      }
      if (/* param.sectorId &&  */param.jobId) {
        // self.currentActiveTab = "Candidate"
        // self.selectedFilter.sector = param.sectorId
        self.selectedjobId = param.jobId
      }
      if (param.sectorId && param.jobId == undefined)
        self.currentActiveTab = "Search"
      console.log(self.currentActiveTab, param.listType);
      self.fetchSectors()
      self.fetchJobs()
      self.fetchStates()
      /* if (param.jobId)
        this.interviewListShow = true */
    });
  }
  activeTab(tabName: string) {
    if (tabName == this.currentActiveTab) {
      return true;
    }
    else {
      return false;
    }
  }
  compare(a: any, b: any) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }
  dynamicSort(property: any) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return (a: any, b: any) => {
      /* next line works with strings and numbers, 
       * and you may want to customize it to your needs
       */
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }
  sortfilterCandidates(sortVal: any) {
    console.log(sortVal);
    this.filteredApplicationsList.sort(this.dynamicSort(sortVal));
  }
  filterCandidates(filterVal: any) {
    if (filterVal == "")
      this.filteredApplicationsList = this.applicationsList
    else {
      this.filteredApplicationsList = this.applicationsList.filter((item: any) => item.statusDetails.interview_status === filterVal)
    }
    console.log(filterVal, this.filteredApplicationsList);
  }
  addRemarkPopup(item: any): void {
    let self = this;
    const dialogRef = this.dialog.open(AddRemarksPopoupComponent, {
      minWidth: '320px', maxWidth: '60%', data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      self.fetchApplications()
    });
  }
  showRemark(remark: string, len: number) {
    return remark?.length >= len ? remark.substring(0, len) + '...' : remark;
  }
  changeState() {
    // console.log(JSON.stringify(this.selectedFilterStateIndex),(this.selectedFilterStateIndex!="" && this.selectedFilterStateIndex!=undefined &&this.selectedFilterStateIndex!=null),this.selectedFilterStateIndex=="");
    if ((this.selectedFilterStateIndex != "" && this.selectedFilterStateIndex != undefined)) {
      this.selectedFilter.state = this.statesList[this.selectedFilterStateIndex - 1]
      this.selectedFilter.district = ""
      this.fetchDistricts();
    }
    else {
      this.selectedFilter.district = ""
      this.selectedFilter.state = { "name": "", "id": "" }
      this.fetchApplications()
    }
  }
  scheduleInterviewPopup(item: any): void {
    let self = this;
    const dialogRef = this.dialog.open(ScheduleInterviewPopoupComponent, {
      minWidth: '320px', maxWidth: '40%', data: { job_id: this.selectedjobId, sector_id: this.selectedFilter.sector, candidate: item },
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      self.fetchApplications()
    });
  }
  updateInterViewStatus(item: any) {
    let self = this;
    console.log(item)
    if (item.statusDetails._id) {
      this.httpService.postWithoutAuth("interviews/update_interview_status", { "_id": item.statusDetails._id, "interview_status": item.statusDetails.interview_status }).subscribe((res: any) => {
        self.fetchApplications();
      });
    }
    else {
      var interviewDetails = {
        "title": "",
        "job_id": this.selectedjobId,
        "job_category_id": "",
        "description": "",
        "recruiter_id": "",
        "candidate_id": item.user,
        "sector_id": this.selectedFilter.sector,
        "job_description_doc": "",
        "salary": "",
        "start_time": "",
        "remark": "",
        "interview_date": "",
        "interview_status": item.statusDetails.interview_status,
        "status": true
      }
      console.log(interviewDetails);

      this.fetchJobDetails(this.selectedjobId).subscribe((res: any) => {
        interviewDetails.job_category_id = res.job_category_id
        interviewDetails.recruiter_id = res.recruiter_id
        interviewDetails.salary = res.salary
        let self = this;
        this.httpService.postWithoutAuth("interviews", interviewDetails).subscribe((res: any) => {
          console.log(res);
          if (res.type) {
            self.httpService.showSuccess("Candidate successfully shortlisted, go to candidates tab to see shortlist.");
            self.fetchApplications()
          }
        });
      });
    }
  }
  fetchJobDetails(jobId: any) {
    return this.httpService.getWithoutAuth("jobs/" + jobId);
  }
  fetchJobs() {
    let self = this;
    if (this.cds.isLoggedIn()) {
      let userInfo = JSON.parse(localStorage.getItem("userInfo")!);
      this.httpService.postWithoutAuth("jobs/recruiter_jobs_list", { "recruiter_id": userInfo._id }).subscribe((res: any) => {
        self.jobsList = res;
        for (let index = 0; index < self.jobsList.length; index++) {
          var shortListed = 0;
          var reviewed = 0;
          var interviewed = 0;
          var onboard = 0;
          self.jobsList[index].interviewData.forEach((element: any) => {
            if (element.interview_status == "Short Listed")
              shortListed++
            else if (element.interview_status == "Resume Reviewed")
              reviewed++
            else if (element.interview_status == "Interview Scheduled")
              interviewed++
            else if (element.interview_status == "Hired")
              onboard++
          });
          self.jobsList[index].shortListed = shortListed
          self.jobsList[index].reviewed = reviewed
          self.jobsList[index].interviewed = interviewed
          self.jobsList[index].onboard = onboard

          // console.log(self.jobsList[index]);          
        }
        // self.sortfilterCandidates(self.selectedCandidateFilter.sortType)
      });
    }
  }
  searchJobByLocation(location: string) {
    console.log(location);
    if (location.length) {
      let self = this;
      if (this.cds.isLoggedIn()) {
        let userInfo = JSON.parse(localStorage.getItem("userInfo")!);
        this.httpService.postWithoutAuth("jobs/searchByaddress", { "recruiter_id": userInfo._id, "address": location }).subscribe((res: any) => {
          self.jobsList = res;
          for (let index = 0; index < self.jobsList.length; index++) {
            var shortListed = 0;
            var reviewed = 0;
            var interviewed = 0;
            var onboard = 0;
            self.jobsList[index].interviewData.forEach((element: any) => {
              if (element.interview_status == "Short Listed")
                shortListed++
              else if (element.interview_status == "Resume Reviewed")
                reviewed++
              else if (element.interview_status == "Interview Scheduled")
                interviewed++
              else if (element.interview_status == "Hired")
                onboard++
            });
            self.jobsList[index].shortListed = shortListed
            self.jobsList[index].reviewed = reviewed
            self.jobsList[index].interviewed = interviewed
            self.jobsList[index].onboard = onboard
            // console.log(self.jobsList[index]);          
          }
          // console.log(self.sectoresList);
        });
      }
    }
    else {
      this.fetchJobs()
    }
    // const res = this.jobsList.filter((obj:any) => Object.values(obj).some((val:any) => val.address(location)));
    /* if(location.length>0){
    const res = this.jobsList.filter((item:any) => {
      let found = false;
      Object.keys(item).forEach(key => {
        if (item[key.toLocaleLowerCase()] && item[key.toLocaleLowerCase()].indexOf && item[key.toLocaleLowerCase()].indexOf(location.toLocaleLowerCase()) > -1) {
          found = true;
        }
      });
      return found;
    });
    res.sort((a: any, b: any) => {
      return a.name - b.name || a.name.localeCompare(b.name);
    });
    
  }
  else{
    
  } */
  }
  fetchSectors() {
    let self = this;
    this.httpService.getL4o("category?categoryType=sector").subscribe((res: any) => {
      self.sectoresList = res.categoryList;
      self.fetchApplications()
      // console.log(self.sectoresList);
    });
  }
  fetchStates() {
    let self = this;
    this.httpService.getL4o("state").subscribe((res: any) => {
      self.statesList = res.state;
      // console.log(self.sectoresList);
    });
  }
  fetchDistricts() {
    let self = this;
    this.httpService.getL4o("state?stateId=" + this.selectedFilter.state.id).subscribe((res: any) => {
      self.districtsList = res.state.districts;
      self.fetchApplications();
      // console.log(self.sectoresList);
    });
  }
  fetchApplications() {
    let self = this;
    console.log(JSON.stringify(this.selectedFilter.state), JSON.stringify({ name: '', id: '' }));

    var reqFilter = "?sector=" + this.selectedFilter.sector + "&state=" + this.selectedFilter.state.name + "&district=" + this.selectedFilter.district + "&userId=" + this.selectedFilter.userId + "&skip&limit";
    self.httpService.getL4o("learner/job" + reqFilter).subscribe((res: any) => {
      self.applicationsList = res.jobApplication;
      self.filteredApplicationsList = res.jobApplication;
      for (let index = 0; index < self.applicationsList.length; index++) {
        // const element = array[index];
        setTimeout(() => {
          if (self.selectedjobId)
            self.getCandidateStatus(self.applicationsList[index], index)

        }, 150);
      }
      self.sortfilterCandidates(self.selectedCandidateFilter.sortType)
      // console.log(self.sectoresList);
    });
  }
  setDropdownVal(val: any) {
    return JSON.parse(JSON.stringify(val))
  }
  getCandidateStatus(CandidateId: any, index: number) {
    let self = this;
    this.httpService.postWithoutAuth("interviews/fetch_candidate_status", { "candidate_id": CandidateId.user, "job_id": this.selectedjobId }).subscribe((res: any) => {
      console.log(res);
      if (res != null) {
        self.applicationsList[index].statusDetails = res;
        self.filteredApplicationsList[index].statusDetails = res;
        self.showNoCandidate++
      }
      else {
        self.applicationsList[index].statusDetails = { interview_status: "Pending" };
        self.filteredApplicationsList[index].statusDetails = { interview_status: "Pending" };
      }
      console.log(self.filteredApplicationsList);
    });
  }
  isCandidateSelectedInJob(candidate: any) {

    var idx = this.jobsList.findIndex((element: any) => element._id == this.selectedjobId);
    var index = this.jobsList[idx].interviewData.findIndex((element: any) => element.candidate_id == candidate.user);
    // console.log(this.jobsList[idx].interviewData,candidate.user);

    if (index < 0)
      return false
    else
      return true
  }
  getUserProfile(item: any) {
    let self = this;
    var reqFilter = "?sector=" + this.selectedFilter.sector + "&state=" + this.selectedFilter.state.name + "&district=" + this.selectedFilter.district + "&userId=" + item.user + "&skip&limit";
    self.httpService.getL4o("learner/job" + reqFilter).subscribe((res: any) => {
      self.selectedProfile = res.jobApplication;
      self.selectedProfile.statusDetails = item.statusDetails;
      self.selectedProfile.selectedjobId = self.selectedjobId;
      self.selectedProfile.selectedFilter = self.selectedFilter;
      self.selectedProfile.videoResume = self.sanitizer.bypassSecurityTrustResourceUrl(this.selectedProfile?.videoResume + '&rel=0');
      self.showProfile = true;
      // self.applicationsList = res.jobApplication;
      // console.log(self.sectoresList);
    });
  }
  updateJob(item: any) {
    let self = this;
    /* item.id=item._id;
    delete item._id;
    delete item.__v; */
    this.httpService.postWithoutAuth("jobs/update", item).subscribe((res: any) => {
      if (res.type) {
        self.httpService.showSuccess(res.message);
        self.fetchJobs();
        // this.router.navigateByUrl("/jobs");
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
  goToCandidateTab() {
    // console.log("Clicked");
    $("#v-pills-messages-tab").click()
  }
  ngOnInit(): void {
    this.fetchSectors()
    this.fetchJobs()
    this.fetchStates()
    /* let arr = [
      { name: "string 1", value: "this", other: "that" },
      { name: "string 2", value: "this", other: "that" },
      { name: "string 3", value: "this", other: "that" },
      { name: "that 4", value: "this", other: "that" },
      { name: "that 5", value: "this", other: "that" },
      { name: "that 6", value: "this", other: "that" },
      { name: "string 2", value: "this", other: "that" }
    ];
    const search = 'string';
    const res = arr.filter(obj => Object.values(obj).some(val => val.includes(search)));
    console.log(res.sort((a: any, b: any) => {
      return a.name - b.name || a.name.localeCompare(b.name);
    })); */
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddWorkExperienceComponent } from '../components/add-work-experience/add-work-experience.component';
import { SaveEducationDialogComponent } from '../components/save-education-dialog/save-education-dialog.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { JobsComponent } from './jobs.component';
import { ProfileComponent } from './profile/profile.component';
import { SaveJobComponent } from './save-job/save-job.component';

const routes: Routes = [
  { path: '',redirectTo: 'dashboard',pathMatch: 'full'},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'dashboard/:listType', component: DashboardComponent, pathMatch: 'full' },
  { path: 'dashboard/jobwisecandidates/:jobId/:sectorId', component: DashboardComponent, pathMatch: 'full' },
  { path: 'dashboard/:listType/:jobId/:sectorId', component: DashboardComponent, pathMatch: 'full' },
  { path: 'create', component:SaveJobComponent},
  { path: 'update/:id', component:SaveJobComponent},
  { path: 'profile', component:ProfileComponent},
  {path: 'addeducation', component:SaveEducationDialogComponent},
  {path: 'addworkexperience', component:AddWorkExperienceComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }

export const apis = {
  learner: {
    addNewCustomer: '/crm/lead/capture',
  },
  auth: {
    signUp: '/signup',
    logIn: '/login',
    forgotPassword: '/forgot-password',
    resetPassword: '/reset-password',
    changePassword: '/update-password'
  },

  shared: {
    // Course apis
    getCourse: '/course-list',
    getCourseById:'/course-list', //check again
    getChapter: '/course/chapter/get-chapter',
    getLecture: '/course/lecture/get-lecture',
    getDiscussion: '/course/discuss',
    getChaptersByCourseId: '/course/syllabus',
    newDiscussion: '/course/discuss/create',
    getQA: '/qa',
    getNotes: '/course/note',
    newQA: '/course/qa/create',
    newNote: '/course/note/create',
    
    // User apis
    updateProfile: '/update-profile',
    getUserInfo:'/get-userInfo',
    getUser: '/user',
    learnerCourses: '/learner/my-courses',
    getInstructor: '/instructors/list',
    addToWaitList: '/create-waitlist',
    userOnboarding: '/learner/details',
    
    // Misc
    addToContactList: '/contact-us',
    validateReferral: '/validate-referral',
    getStates: '/state',
    getCoupon: '/coupon-search',
    
    // Payments
    initiatePayment:'/course/initiate-payment',
    verifyPayment:'/payment/verify-payment'
  },
};
